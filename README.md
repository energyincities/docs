This project contains documents used in the organisation of the Evins research group, including:
* [Standard Operating Procedures](/sops/list.md)
* Individual [working documents](/individual/list.md) for each group member